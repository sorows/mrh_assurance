package MASM.Entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "ContratEntity")
public class ContratEntity {
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String libelle;
	
	@JsonFormat(pattern = "dd/MM/yyyy")
	private Date date_debut;
	
	@JsonFormat(pattern = "dd/MM/yyyy")
	private Date date_fin;
	
	@OneToMany(mappedBy = "contratentity")
	private List<RapportEntity> RapportEntity;
	
	@ManyToOne
	@JoinColumn(name = "CODE_TYPE_CONTRAT")
	private TypeContratEntity typecontratentity;
	
	public ContratEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public ContratEntity(String libelle, Date date_debut, Date date_fin, TypeContratEntity typecontratentity) {
		super();
		this.libelle = libelle;
		this.date_debut = date_debut;
		this.date_fin = date_fin;
		this.typecontratentity = typecontratentity;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public Date getDate_debut() {
		return date_debut;
	}
	public void setDate_debut(Date date_debut) {
		this.date_debut = date_debut;
	}
	public Date getDate_fin() {
		return date_fin;
	}
	public void setDate_fin(Date date_fin) {
		this.date_fin = date_fin;
	}
	public TypeContratEntity getTypecontratentity() {
		return typecontratentity;
	}
	public void setTypecontratentity(TypeContratEntity typecontratentity) {
		this.typecontratentity = typecontratentity;
	}
	
	;

}
