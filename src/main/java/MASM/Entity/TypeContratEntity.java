package MASM.Entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class TypeContratEntity {
		
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String libelle;
	
	@OneToMany(mappedBy = "typecontratentity")
	private List<SinistreEntity> sinistreentity;
	
	@OneToMany(mappedBy = "typecontratentity")
	private List<ContratEntity> contratEntity;
		
	public TypeContratEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TypeContratEntity(String libelle) {
		super();
		this.libelle = libelle;
	}



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getLibelle() {
		return libelle;
	}



	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	

	
	

}
