package MASM.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "NewsLettersEntity")
public class NewsLettersEntity {
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	private Long id;
	private String mail;
	public NewsLettersEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	public NewsLettersEntity(String mail) {
		super();
		this.mail = mail;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	

}
