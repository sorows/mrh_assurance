package MASM.Entity;

import java.io.UnsupportedEncodingException;
import java.lang.invoke.MethodHandles.Lookup;
import java.nio.charset.Charset;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "SinistreEntity")
public class SinistreEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String libelle;
	private String avantage;
	
	@ManyToOne
	@JoinColumn(name = "TypeContratEntity")
	private TypeContratEntity typecontratentity;

	public SinistreEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SinistreEntity(String libelle, String avantage, TypeContratEntity typecontratentity) {
		super();
		this.libelle = libelle;
		this.avantage = avantage;
		this.typecontratentity = typecontratentity;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getAvantage() {
		return avantage;
	}

	public void setAvantage(String avantage) {
		this.avantage = avantage;
	}

	public TypeContratEntity getTypecontratentity() {
		return typecontratentity;
	}

	public void setTypecontratentity(TypeContratEntity typecontratentity) {
		this.typecontratentity = typecontratentity;
	}

	public int length() {
		return avantage.length();
	}

	public boolean isEmpty() {
		return avantage.isEmpty();
	}

	public char charAt(int index) {
		return avantage.charAt(index);
	}

	public int codePointAt(int index) {
		return avantage.codePointAt(index);
	}

	public int codePointBefore(int index) {
		return avantage.codePointBefore(index);
	}

	public int codePointCount(int beginIndex, int endIndex) {
		return avantage.codePointCount(beginIndex, endIndex);
	}

	public int offsetByCodePoints(int index, int codePointOffset) {
		return avantage.offsetByCodePoints(index, codePointOffset);
	}

	public void getChars(int srcBegin, int srcEnd, char[] dst, int dstBegin) {
		avantage.getChars(srcBegin, srcEnd, dst, dstBegin);
	}

	public void getBytes(int srcBegin, int srcEnd, byte[] dst, int dstBegin) {
		avantage.getBytes(srcBegin, srcEnd, dst, dstBegin);
	}

	public byte[] getBytes(String charsetName) throws UnsupportedEncodingException {
		return avantage.getBytes(charsetName);
	}

	public byte[] getBytes(Charset charset) {
		return avantage.getBytes(charset);
	}

	public byte[] getBytes() {
		return avantage.getBytes();
	}

	public boolean equals(Object anObject) {
		return avantage.equals(anObject);
	}

	public boolean contentEquals(StringBuffer sb) {
		return avantage.contentEquals(sb);
	}

	public boolean contentEquals(CharSequence cs) {
		return avantage.contentEquals(cs);
	}

	public boolean equalsIgnoreCase(String anotherString) {
		return avantage.equalsIgnoreCase(anotherString);
	}

	public int compareTo(String anotherString) {
		return avantage.compareTo(anotherString);
	}

	public int compareToIgnoreCase(String str) {
		return avantage.compareToIgnoreCase(str);
	}

	public boolean regionMatches(int toffset, String other, int ooffset, int len) {
		return avantage.regionMatches(toffset, other, ooffset, len);
	}

	public boolean regionMatches(boolean ignoreCase, int toffset, String other, int ooffset, int len) {
		return avantage.regionMatches(ignoreCase, toffset, other, ooffset, len);
	}

	public boolean startsWith(String prefix, int toffset) {
		return avantage.startsWith(prefix, toffset);
	}

	public boolean startsWith(String prefix) {
		return avantage.startsWith(prefix);
	}

	public boolean endsWith(String suffix) {
		return avantage.endsWith(suffix);
	}

	public int hashCode() {
		return avantage.hashCode();
	}

	public int indexOf(int ch) {
		return avantage.indexOf(ch);
	}

	public int indexOf(int ch, int fromIndex) {
		return avantage.indexOf(ch, fromIndex);
	}

	public int lastIndexOf(int ch) {
		return avantage.lastIndexOf(ch);
	}

	public int lastIndexOf(int ch, int fromIndex) {
		return avantage.lastIndexOf(ch, fromIndex);
	}

	public int indexOf(String str) {
		return avantage.indexOf(str);
	}

	public int indexOf(String str, int fromIndex) {
		return avantage.indexOf(str, fromIndex);
	}

	public int lastIndexOf(String str) {
		return avantage.lastIndexOf(str);
	}

	public int lastIndexOf(String str, int fromIndex) {
		return avantage.lastIndexOf(str, fromIndex);
	}

	public String substring(int beginIndex) {
		return avantage.substring(beginIndex);
	}

	public String substring(int beginIndex, int endIndex) {
		return avantage.substring(beginIndex, endIndex);
	}

	public CharSequence subSequence(int beginIndex, int endIndex) {
		return avantage.subSequence(beginIndex, endIndex);
	}

	public String concat(String str) {
		return avantage.concat(str);
	}

	public String replace(char oldChar, char newChar) {
		return avantage.replace(oldChar, newChar);
	}

	public boolean matches(String regex) {
		return avantage.matches(regex);
	}

	public boolean contains(CharSequence s) {
		return avantage.contains(s);
	}

	public String replaceFirst(String regex, String replacement) {
		return avantage.replaceFirst(regex, replacement);
	}

	public String replaceAll(String regex, String replacement) {
		return avantage.replaceAll(regex, replacement);
	}

	public String replace(CharSequence target, CharSequence replacement) {
		return avantage.replace(target, replacement);
	}

	public String[] split(String regex, int limit) {
		return avantage.split(regex, limit);
	}

	public String[] split(String regex) {
		return avantage.split(regex);
	}

	public String toLowerCase(Locale locale) {
		return avantage.toLowerCase(locale);
	}

	public String toLowerCase() {
		return avantage.toLowerCase();
	}

	public String toUpperCase(Locale locale) {
		return avantage.toUpperCase(locale);
	}

	public String toUpperCase() {
		return avantage.toUpperCase();
	}

	public String trim() {
		return avantage.trim();
	}

	public String strip() {
		return avantage.strip();
	}

	public String stripLeading() {
		return avantage.stripLeading();
	}

	public String stripTrailing() {
		return avantage.stripTrailing();
	}

	public boolean isBlank() {
		return avantage.isBlank();
	}

	public Stream<String> lines() {
		return avantage.lines();
	}

	public String indent(int n) {
		return avantage.indent(n);
	}

	public String stripIndent() {
		return avantage.stripIndent();
	}

	public String translateEscapes() {
		return avantage.translateEscapes();
	}

	public <R> R transform(Function<? super String, ? extends R> f) {
		return avantage.transform(f);
	}

	public String toString() {
		return avantage.toString();
	}

	public IntStream chars() {
		return avantage.chars();
	}

	public IntStream codePoints() {
		return avantage.codePoints();
	}

	public char[] toCharArray() {
		return avantage.toCharArray();
	}

	public String formatted(Object... args) {
		return avantage.formatted(args);
	}

	public String intern() {
		return avantage.intern();
	}

	public String repeat(int count) {
		return avantage.repeat(count);
	}

	public Optional<String> describeConstable() {
		return avantage.describeConstable();
	}

	public String resolveConstantDesc(Lookup lookup) {
		return avantage.resolveConstantDesc(lookup);
	}
	
	
	
	

}
