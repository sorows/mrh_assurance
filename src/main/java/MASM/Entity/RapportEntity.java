package MASM.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "RapportEntity")
public class RapportEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String libelle;
	
	@ManyToOne
	@JoinColumn(name = "CODE_PERSONNEL")
	private PersonnelEntity personnelentity;
	
	@ManyToOne
	@JoinColumn(name = "CODE_CONTRAT")
	private ContratEntity contratentity;
	
	
	public RapportEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public RapportEntity(String libelle, PersonnelEntity personnelentity, ContratEntity contratentity) {
		super();
		this.libelle = libelle;
		this.personnelentity = personnelentity;
		this.contratentity = contratentity;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getLibelle() {
		return libelle;
	}


	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}


	public PersonnelEntity getPersonnelEntity() {
		return personnelentity;
	}


	public void setPersonnelEntity(PersonnelEntity personnelEntity) {
		this.personnelentity = personnelEntity;
	}


	public ContratEntity getContratEntity() {
		return contratentity;
	}


	public void setContratEntity(ContratEntity contratEntity) {
		this.contratentity = contratEntity;
	}


	public int hashCode() {
		return contratentity.hashCode();
	}
	public boolean equals(Object obj) {
		return contratentity.equals(obj);
	}
	public String toString() {
		return contratentity.toString();
	}
	
	
	
	
	
	
	
	

}
