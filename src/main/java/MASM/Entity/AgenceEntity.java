package MASM.Entity;


import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "AgenceEntity")
public class AgenceEntity {
	
    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id ;
	private String libelle ;
	
	@OneToMany(mappedBy = "agenceentity")
	private List<PersonnelEntity> personnelentity;
	
	public AgenceEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AgenceEntity(String libelle) {
		super();
		this.libelle = libelle;
	}
	public Long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	

	

	
	
	
	
}
