package MASM.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ServicePersonnelEntity")
public class ServicePersonnelEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "CODE_SERVICE")
	private ServiceEntity serviceentity;
	
	@ManyToOne
	@JoinColumn(name = "CODE_PERSONNEL")
	private PersonnelEntity personnelentity;
	
	public ServicePersonnelEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public ServicePersonnelEntity(ServiceEntity serviceentity, PersonnelEntity personnelentity) {
		super();
		this.serviceentity = serviceentity;
		this.personnelentity = personnelentity;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public ServiceEntity getServiceentity() {
		return serviceentity;
	}
	public void setServiceentity(ServiceEntity serviceentity) {
		this.serviceentity = serviceentity;
	}
	public PersonnelEntity getPersonnelentity() {
		return personnelentity;
	}
	public void setPersonnelentity(PersonnelEntity personnelentity) {
		this.personnelentity = personnelentity;
	}
	
	

}
