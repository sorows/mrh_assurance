package MASM.Entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ClientEntity")
public class ClientEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO )
	private Long id;
	private String nom;
	private String prenom ;
	private String contact;
	private String adress;
	private String profession ;
	private Date date_naissance ;
	private String lieu_naissance;
	private String numero_cni;
	public ClientEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ClientEntity(String nom, String prenom, String contact, String adress, String profession,
			Date date_naissance, String lieu_naissance, String numero_cni) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.contact = contact;
		this.adress = adress;
		this.profession = profession;
		this.date_naissance = date_naissance;
		this.lieu_naissance = lieu_naissance;
		this.numero_cni = numero_cni;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public String getProfession() {
		return profession;
	}
	public void setProfession(String profession) {
		this.profession = profession;
	}
	public Date getDate_naissance() {
		return date_naissance;
	}
	public void setDate_naissance(Date date_naissance) {
		this.date_naissance = date_naissance;
	}
	public String getLieu_naissance() {
		return lieu_naissance;
	}
	public void setLieu_naissance(String lieu_naissance) {
		this.lieu_naissance = lieu_naissance;
	}
	public String getNumero_cni() {
		return numero_cni;
	}
	public void setNumero_cni(String numero_cni) {
		this.numero_cni = numero_cni;
	}
	
	
	
	
	
}
