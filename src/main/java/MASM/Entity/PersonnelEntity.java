package MASM.Entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "PersonnelEntity")
public class PersonnelEntity {
	
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO )
	private Long id;
	private String nom;
	private String prenom;
	private String adress;
	private String contact;
	private String numero_cni;
	private String photo_personnel;
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date date_naissance;
	private String lieu_naissance;
	
	@OneToMany(mappedBy = "personnelentity")
	private List<RapportEntity> RapportEntity;
	
	@OneToMany(mappedBy = "personnelentity")
	private List<ServicePersonnelEntity> servicepersonnelentity;
	
	@ManyToOne
	@JoinColumn(name = "CODE_ROLE")
	private RoleEntity roleentity;
	
	@ManyToOne
	@JoinColumn(name = "CODE_AGENCE")	
	private AgenceEntity agenceentity;
	
	public PersonnelEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PersonnelEntity(String nom, String prenom, String adress, String contact, String numero_cni,
			String photo_personnel, Date date_naissance, String lieu_naissance, RoleEntity roleentity,
			AgenceEntity agenceentity) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.adress = adress;
		this.contact = contact;
		this.numero_cni = numero_cni;
		this.photo_personnel = photo_personnel;
		this.date_naissance = date_naissance;
		this.lieu_naissance = lieu_naissance;
		this.roleentity = roleentity;
		this.agenceentity = agenceentity;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getNumero_cni() {
		return numero_cni;
	}
	public void setNumero_cni(String numero_cni) {
		this.numero_cni = numero_cni;
	}
	public String getPhoto_personnel() {
		return photo_personnel;
	}
	public void setPhoto_personnel(String photo_personnel) {
		this.photo_personnel = photo_personnel;
	}
	public Date getDate_naissance() {
		return date_naissance;
	}
	public void setDate_naissance(Date date_naissance) {
		this.date_naissance = date_naissance;
	}
	public String getLieu_naissance() {
		return lieu_naissance;
	}
	public void setLieu_naissance(String lieu_naissance) {
		this.lieu_naissance = lieu_naissance;
	}
	public RoleEntity getRoleentity() {
		return roleentity;
	}
	public void setRoleentity(RoleEntity roleentity) {
		this.roleentity = roleentity;
	}
	public AgenceEntity getAngenceentity() {
		return agenceentity;
	}
	public void setAngenceentity(AgenceEntity angenceentity) {
		this.agenceentity = angenceentity;
	}
	

	
	

	
	
	

}
