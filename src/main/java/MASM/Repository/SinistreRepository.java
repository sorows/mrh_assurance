package MASM.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import MASM.Entity.SinistreEntity;

@Repository
public interface SinistreRepository  extends JpaRepository<SinistreEntity, Long> {

}
