package MASM.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import MASM.Entity.TypeContratEntity;

@Repository
public interface TypeContratRepository extends JpaRepository<TypeContratEntity, Long> {

}
