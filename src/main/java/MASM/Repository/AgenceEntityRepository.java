package MASM.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import MASM.Entity.AgenceEntity;

@Repository
public interface AgenceEntityRepository  extends  JpaRepository<AgenceEntity, Long>{
	
	/*
	@Modifying
	@Query(value=" update AgenceEntity u set u.libelle = :libelle where u.id = :id ")
	public void updateAgenceById(@Param("libelle") String libelle, @Param("id") Long id);
	*/
}
