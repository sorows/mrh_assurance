package MASM.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import MASM.Entity.RapportEntity;

@Repository
public interface RapportRepository extends JpaRepository<RapportEntity, Long>  {

}
