package MASM.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import MASM.Entity.ServicePersonnelEntity;

@Repository
public interface ServicePersonnelRepository extends JpaRepository<ServicePersonnelEntity, Long> {

}
