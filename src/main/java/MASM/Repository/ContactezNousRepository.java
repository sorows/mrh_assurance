package MASM.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import MASM.Entity.ContactezNousEntity;

@Repository
public interface ContactezNousRepository extends JpaRepository<ContactezNousEntity, Long> {

}
