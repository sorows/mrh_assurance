package MASM.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import MASM.Entity.ContratEntity;

@Repository
public interface ContratRepository extends JpaRepository<ContratEntity, Long> {

}
