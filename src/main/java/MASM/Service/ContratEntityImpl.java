package MASM.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import MASM.Entity.ContratEntity;
import MASM.Repository.ContratRepository;

@Component
public class ContratEntityImpl implements IContratEntity{
	
	@Autowired
	ContratRepository contrat;

	@Override
	public ContratEntity createContrat(ContratEntity ce) {
		// TODO Auto-generated method stub
		return contrat.save(ce);
	}

	@Override
	public List<ContratEntity> readContrat() {
		// TODO Auto-generated method stub
		return contrat.findAll();
	}

	@Override
	public ContratEntity updateContratById(Long id, ContratEntity ce) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteContratById(Long id) {
		contrat.deleteById(id);
	}

		

}
