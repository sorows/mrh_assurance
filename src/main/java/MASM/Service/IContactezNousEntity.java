package MASM.Service;

import java.util.List;

import MASM.Entity.ContactezNousEntity;

public interface IContactezNousEntity {
	
	public ContactezNousEntity createContactez(ContactezNousEntity cne);
	public List<ContactezNousEntity> readContactez();

}
