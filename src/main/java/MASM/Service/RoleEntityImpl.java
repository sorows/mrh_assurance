package MASM.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import MASM.Entity.RoleEntity;
import MASM.Repository.RoleRepository;

@Component
public class RoleEntityImpl implements IRoleEntity{
	
	@Autowired
	RoleRepository role;

	@Override
	public RoleEntity createRole(RoleEntity re) {
		// TODO Auto-generated method stub
		return role.save(re);
	}

	@Override
	public List<RoleEntity> readRole() {
		// TODO Auto-generated method stub
		return role.findAll();
	}

	@Override
	public RoleEntity updateRoleById(Long id, RoleEntity re) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteRoleById(Long id) {
		// TODO Auto-generated method stub
		role.deleteById(id);
	}
	
	

}
