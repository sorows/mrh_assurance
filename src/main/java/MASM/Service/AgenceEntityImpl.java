package MASM.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import MASM.Entity.AgenceEntity;
import MASM.Repository.AgenceEntityRepository;

@Component
public class AgenceEntityImpl implements IAgenceEntity{
	
	@Autowired
	AgenceEntityRepository agence; 

	@Override
	public AgenceEntity createAgence(AgenceEntity ag) {
		
		return agence.save(ag);
		
	}

	@Override
	public List<AgenceEntity> readAgence() {
		return agence.findAll();
	}

	/*
	@Override
	public AgenceEntity updateAgenceById(AgenceEntity ag) {
		
		return agence.save(ag);
		
	}
	*/
	
	@Override
	public void deleteAgenceById(Long id) {
		
		agence.deleteById(id);
		
	}
	
	@Transactional
	public void updateAgenceById(AgenceEntity ag) {
		
		
		
	}

	@Override
	public AgenceEntity updateAgenceById(Long id, AgenceEntity ag) {
		
		return null;
	}
	

}
