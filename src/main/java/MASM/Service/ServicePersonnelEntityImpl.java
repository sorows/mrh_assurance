package MASM.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import MASM.Entity.ServicePersonnelEntity;
import MASM.Repository.ServicePersonnelRepository;

@Component
public class ServicePersonnelEntityImpl implements IServicePersonnelEntity{

	@Autowired
	ServicePersonnelRepository service;
	
	@Override
	public ServicePersonnelEntity createServicePersonnel(ServicePersonnelEntity spe) {
		// TODO Auto-generated method stub
		return service.save(spe);
	}

	@Override
	public List<ServicePersonnelEntity> readServicePersonnel() {
		// TODO Auto-generated method stub
		return service.findAll();
	}

	@Override
	public ServicePersonnelEntity updateServicePersonnelById(Long id, ServicePersonnelEntity spe) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteServicePersonnelById(Long id) {
		// TODO Auto-generated method stub
		service.deleteById(id);
	}
	
	

}
