package MASM.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import MASM.Entity.ContactezNousEntity;
import MASM.Repository.ContactezNousRepository;

@Component
public class ContactezNousEntityImpl implements IContactezNousEntity {

	@Autowired
	ContactezNousRepository contact;
	
	@Override
	public ContactezNousEntity createContactez(ContactezNousEntity cne) {
		// TODO Auto-generated method stub
		return contact.save(cne);
	}

	@Override
	public List<ContactezNousEntity> readContactez() {
		// TODO Auto-generated method stub
		return contact.findAll();
	}
	
	

}
