package MASM.Service;

import java.util.List;

import MASM.Entity.ClientEntity;

public interface IClientEntity {
	
	public ClientEntity createClient(ClientEntity ag);
	public List<ClientEntity> readClient();
	public ClientEntity updateClientById(Long id, ClientEntity ag);
	public void deleteClientById(Long id);

}
