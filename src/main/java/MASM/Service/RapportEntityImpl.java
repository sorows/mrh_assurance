package MASM.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import MASM.Entity.RapportEntity;
import MASM.Repository.RapportRepository;

@Component
public class RapportEntityImpl implements IRapportEntity{

	@Autowired
	RapportRepository rapport;
	
	
	@Override
	public RapportEntity createRapport(RapportEntity re) {
		// TODO Auto-generated method stub
		return rapport.save(re);
	}

	@Override
	public List<RapportEntity> readRapport() {
		// TODO Auto-generated method stub
		return rapport.findAll();
	}

	@Override
	public RapportEntity updateRapportById(Long id, RapportEntity ce) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteRapportById(Long id) {
		// TODO Auto-generated method stub
		rapport.deleteById(id);
	}

		

}
