package MASM.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import MASM.Entity.PersonnelEntity;
import MASM.Repository.PersonnelRepository;

@Component
public class PersonnelEntityImpl implements IPersonnelEntity{

	@Autowired
	PersonnelRepository personnel;
	
	@Override
	public PersonnelEntity createPersonnel(PersonnelEntity pe) {
		// TODO Auto-generated method stub
		return personnel.save(pe);
	}

	@Override
	public List<PersonnelEntity> readPersonnel() {
		// TODO Auto-generated method stub
		return personnel.findAll();
	}

	@Override
	public PersonnelEntity updatePersonnelById(Long id, PersonnelEntity pe) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deletePersonnelById(Long id) {
		// TODO Auto-generated method stub
		personnel.deleteById(id);
	}

	
	
	

}
