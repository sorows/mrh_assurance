package MASM.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import MASM.Entity.TypeContratEntity;
import MASM.Repository.TypeContratRepository;

@Component
public class TypeContratEntityImpl implements ITypeContratEntity{
	
	@Autowired
	TypeContratRepository typeContrat;

	@Override
	public TypeContratEntity createTypeContrat(TypeContratEntity tce) {
		// TODO Auto-generated method stub
		return typeContrat.save(tce);
	}

	@Override
	public List<TypeContratEntity> readTypeContrat() {
		// TODO Auto-generated method stub
		return typeContrat.findAll();
	}

	@Override
	public TypeContratEntity updateTypeContratById(Long id, TypeContratEntity tce) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteTypeContratById(Long id) {
		// TODO Auto-generated method stub
		typeContrat.deleteById(id);
	}
	
	

}
