package MASM.Service;

import java.util.List;

import MASM.Entity.RoleEntity;

public interface IRoleEntity {
	
	public RoleEntity createRole(RoleEntity re);
	public List<RoleEntity> readRole();
	public RoleEntity updateRoleById(Long id, RoleEntity re);
	public void deleteRoleById(Long id);

}
