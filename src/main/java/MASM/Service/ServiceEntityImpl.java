package MASM.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import MASM.Entity.ServiceEntity;
import MASM.Repository.ServiceRepository;

@Component
public class ServiceEntityImpl implements IServiceEntity{

	@Autowired
	ServiceRepository service;
	
	@Override
	public ServiceEntity createService(ServiceEntity se) {
		// TODO Auto-generated method stub
		return service.save(se);
	}

	@Override
	public List<ServiceEntity> readService() {
		// TODO Auto-generated method stub
		return service.findAll();
	}

	@Override
	public ServiceEntity updateServiceById(Long id, ServiceEntity se) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteServiceById(Long id) {
		// TODO Auto-generated method stub
		service.deleteById(id);
	}

		

}
