package MASM.Service;

import java.util.List;

import MASM.Entity.PersonnelEntity;

public interface IPersonnelEntity {
	
	public PersonnelEntity createPersonnel(PersonnelEntity pe);
	public List<PersonnelEntity> readPersonnel();
	public PersonnelEntity updatePersonnelById(Long id, PersonnelEntity pe);
	public void deletePersonnelById(Long id);

}
