package MASM.Service;

import java.util.List;

import MASM.Entity.SinistreEntity;

public interface ISinistreEntity {

	public SinistreEntity createSinistre(SinistreEntity se);
	public List<SinistreEntity> readSinistre();
	public SinistreEntity updateSinistreById(Long id, SinistreEntity se);
	public void deleteSinistreById(Long id);

}
