package MASM.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import MASM.Entity.ClientEntity;
import MASM.Repository.ClientRepository;

@Component
public class ClientEntityImpl implements IClientEntity{

	@Autowired
	ClientRepository client;

	@Override
	public ClientEntity createClient(ClientEntity ag) {
		// TODO Auto-generated method stub
		return client.save(ag);
	}

	@Override
	public List<ClientEntity> readClient() {
		// TODO Auto-generated method stub
		return client.findAll();
	}

	@Override
	public ClientEntity updateClientById(Long id, ClientEntity ag) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteClientById(Long id) {
		// TODO Auto-generated method stub
		client.deleteById(id);
	}
	
	

		

}
