package MASM.Service;

import java.util.List;

import MASM.Entity.ContratEntity;

public interface IContratEntity {
	
	public ContratEntity createContrat(ContratEntity ce);
	public List<ContratEntity> readContrat();
	public ContratEntity updateContratById(Long id, ContratEntity ce);
	public void deleteContratById(Long id);

}
