package MASM.Service;

import java.util.List;

import MASM.Entity.AgenceEntity;

public interface IAgenceEntity {
	
	public AgenceEntity createAgence(AgenceEntity ag);
	public List<AgenceEntity> readAgence();
	public AgenceEntity updateAgenceById(Long id, AgenceEntity ag);
	public void deleteAgenceById(Long id);

}
