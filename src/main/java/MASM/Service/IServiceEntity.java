package MASM.Service;

import java.util.List;

import MASM.Entity.ServiceEntity;

public interface IServiceEntity {
	

	public ServiceEntity createService(ServiceEntity se);
	public List<ServiceEntity> readService();
	public ServiceEntity updateServiceById(Long id, ServiceEntity se);
	public void deleteServiceById(Long id);

}
