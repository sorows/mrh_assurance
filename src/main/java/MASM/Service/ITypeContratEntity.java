package MASM.Service;

import java.util.List;

import MASM.Entity.TypeContratEntity;

public interface ITypeContratEntity {
	
	public TypeContratEntity createTypeContrat(TypeContratEntity tce);
	public List<TypeContratEntity> readTypeContrat();
	public TypeContratEntity updateTypeContratById(Long id, TypeContratEntity tce);
	public void deleteTypeContratById(Long id);

}
