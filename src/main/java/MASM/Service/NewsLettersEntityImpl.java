package MASM.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import MASM.Entity.NewsLettersEntity;
import MASM.Repository.NewsLetttersRepository;

@Component
public class NewsLettersEntityImpl implements INewsLettersEntity{

	@Autowired
	NewsLetttersRepository news;
	
	@Override
	public NewsLettersEntity createNewsLetter(NewsLettersEntity ne) {
		// TODO Auto-generated method stub
		return news.save(ne);
	}

	@Override
	public List<NewsLettersEntity> readNewsLetter() {
		// TODO Auto-generated method stub
		return news.findAll();
	}

		

}
