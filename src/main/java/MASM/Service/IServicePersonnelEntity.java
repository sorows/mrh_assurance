package MASM.Service;

import java.util.List;

import MASM.Entity.ServicePersonnelEntity;

public interface IServicePersonnelEntity {
	
	public ServicePersonnelEntity createServicePersonnel(ServicePersonnelEntity spe);
	public List<ServicePersonnelEntity> readServicePersonnel();
	public ServicePersonnelEntity updateServicePersonnelById(Long id, ServicePersonnelEntity spe);
	public void deleteServicePersonnelById(Long id);


}
