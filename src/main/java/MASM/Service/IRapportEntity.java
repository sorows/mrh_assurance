package MASM.Service;

import java.util.List;

import MASM.Entity.RapportEntity;

public interface IRapportEntity {
	
	public RapportEntity createRapport(RapportEntity ce);
	public List<RapportEntity> readRapport();
	public RapportEntity updateRapportById(Long id, RapportEntity ce);
	public void deleteRapportById(Long id);

}
