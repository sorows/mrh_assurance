package MASM.Service;

import java.util.List;

import MASM.Entity.NewsLettersEntity;

public interface INewsLettersEntity {
	
	public NewsLettersEntity createNewsLetter(NewsLettersEntity ce);
	public List<NewsLettersEntity> readNewsLetter();

}
