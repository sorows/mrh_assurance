package MASM.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import MASM.Entity.SinistreEntity;
import MASM.Repository.SinistreRepository;

@Component
public class SinistreEntityImpl implements ISinistreEntity{
	
	@Autowired
	SinistreRepository sinistre;

	@Override
	public SinistreEntity createSinistre(SinistreEntity se) {
		// TODO Auto-generated method stub
		return sinistre.save(se);
	}

	@Override
	public List<SinistreEntity> readSinistre() {
		// TODO Auto-generated method stub
		return sinistre.findAll();
	}

	@Override
	public SinistreEntity updateSinistreById(Long id, SinistreEntity se) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteSinistreById(Long id) {
		// TODO Auto-generated method stub
		sinistre.deleteById(id);
	}

		

}
