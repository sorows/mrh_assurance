package MASM.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import MASM.Entity.TypeContratEntity;
import MASM.Repository.TypeContratRepository;
import MASM.Service.TypeContratEntityImpl;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class TypeContratController {
	
	@Autowired
	TypeContratRepository typeContrat;
	
	@Autowired
	TypeContratEntityImpl IContratEntity;
	
	@RequestMapping(value="/ajouterTypeContrat", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public TypeContratEntity ajouterTypeContrat(@RequestBody TypeContratEntity tce) {
		return IContratEntity.createTypeContrat(tce);
	}
	
	@RequestMapping(value="/listeTypeContrat", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public List<TypeContratEntity> listeTypeContrat() {
		return IContratEntity.readTypeContrat();
	}
	
	@PutMapping(value="/updateTypeContrat/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@Transactional
	public TypeContratEntity updateTypeContratById(@RequestBody TypeContratEntity tce, @PathVariable Long id) {
		
		tce.setId(id);
		typeContrat.save(tce);
		return tce;
		//return "Record updated successfully using @Modifiying and @query Named Parameter";
	
	}
	
	@GetMapping(value="/afficherUnTypeContrat/{id}")
	public TypeContratEntity afficherUnTypeContrat(@PathVariable Long id){
		
		TypeContratEntity tce = typeContrat.findById(id).orElse(null);
		
		return tce;
		
	}
	
	@DeleteMapping(value="/deleteUnTypeContrat/{id}")
	public void deleteUnTypeContrat(@PathVariable Long id) {
		IContratEntity.deleteTypeContratById(id);
	}

}
