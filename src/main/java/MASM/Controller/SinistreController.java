package MASM.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import MASM.Entity.SinistreEntity;
import MASM.Repository.SinistreRepository;
import MASM.Service.SinistreEntityImpl;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class SinistreController {

	@Autowired
	SinistreRepository sinistre ;
	
	@Autowired
	SinistreEntityImpl ISinistre;
	
	@RequestMapping(value="/ajouterSinistre", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public SinistreEntity ajouterSinistre(@RequestBody SinistreEntity se) {
		return ISinistre.createSinistre(se);
	}
	
	@RequestMapping(value="/listeSinistre", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public List<SinistreEntity> listeContrat() {
		return ISinistre.readSinistre();
	}
	
	@PutMapping(value="/updateSinistreById/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@Transactional
	public SinistreEntity updateSinistreById(@RequestBody SinistreEntity se, @PathVariable Long id) {
		
		se.setId(id);
		sinistre.save(se);
		return se;
		//return "Record updated successfully using @Modifiying and @query Named Parameter";
	
	}
	
	@GetMapping(value="/afficherUnSinistre/{id}")
	public SinistreEntity afficherUnContrat(@PathVariable Long id){
		
		SinistreEntity se = sinistre.findById(id).orElse(null);
		
		return se;
		
	}
	
	@DeleteMapping(value="/deleteUnSinistre/{id}")
	public void deleteUnContrat(@PathVariable Long id) {
		ISinistre.deleteSinistreById(id);
	}
	
}
