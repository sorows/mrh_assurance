package MASM.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import MASM.Entity.RapportEntity;
import MASM.Repository.RapportRepository;
import MASM.Service.RapportEntityImpl;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class RapportController {
	
	@Autowired
	RapportRepository rapport;
	
	@Autowired
	RapportEntityImpl IRapport;
	
	@RequestMapping(value="/ajouterRapport", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public RapportEntity ajouterRapport(@RequestBody RapportEntity re) {
		return IRapport.createRapport(re);
	}
	
	@RequestMapping(value="/listeRapport", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public List<RapportEntity> listeRapport() {
		return IRapport.readRapport();
	}
	
	@PutMapping(value="/updateRapportById/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@Transactional
	public RapportEntity updateRapportById(@RequestBody RapportEntity re, @PathVariable Long id) {
		
		re.setId(id);
		rapport.save(re);
		return re;
		//return "Record updated successfully using @Modifiying and @query Named Parameter";
	
	}
	
	@GetMapping(value="/afficherUnRapport/{id}")
	public RapportEntity afficherUnContrat(@PathVariable Long id){
		
		RapportEntity ce = rapport.findById(id).orElse(null);
		
		return ce;
		
	}
	
	@DeleteMapping(value="/deleteUnRapport/{id}")
	public void deleteUnRapport(@PathVariable Long id) {
		IRapport.deleteRapportById(id);
	}

}
