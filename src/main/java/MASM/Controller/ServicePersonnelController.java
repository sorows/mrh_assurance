package MASM.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import MASM.Entity.ServicePersonnelEntity;
import MASM.Repository.ServicePersonnelRepository;
import MASM.Service.ServicePersonnelEntityImpl;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ServicePersonnelController {

	@Autowired
	ServicePersonnelRepository service;
	
	@Autowired
	ServicePersonnelEntityImpl IService;
	
	@RequestMapping(value="/ajouterServicePersonnel", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ServicePersonnelEntity ajouterServicePersonnel(@RequestBody ServicePersonnelEntity spe) {
		return IService.createServicePersonnel(spe);
	}
	
	@RequestMapping(value="/listeServicePersonnel", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public List<ServicePersonnelEntity> listeServicePersonnel() {
		return IService.readServicePersonnel();
	}
	
	@PutMapping(value="/updateServicePersonnelById/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@Transactional
	public ServicePersonnelEntity updateServicePersonnelById(@RequestBody ServicePersonnelEntity spe, @PathVariable Long id) {
		
		spe.setId(id);
		service.save(spe);
		return spe;
		//return "Record updated successfully using @Modifiying and @query Named Parameter";
	
	}
	
	@GetMapping(value="/afficherUnServicePersonnel/{id}")
	public ServicePersonnelEntity afficherUnServicePersonnel(@PathVariable Long id){
		
		ServicePersonnelEntity spe = service.findById(id).orElse(null);
		
		return spe;
		
	}
	
	@DeleteMapping(value="/deleteUnServicePersonnel/{id}")
	public void deleteUnServicePersonnel(@PathVariable Long id) {
		IService.deleteServicePersonnelById(id);
	}
	
}
