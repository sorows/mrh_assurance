package MASM.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import MASM.Entity.RoleEntity;
import MASM.Repository.RoleRepository;
import MASM.Service.RoleEntityImpl;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class RoleController {

	@Autowired
	RoleRepository role;
	
	@Autowired
	RoleEntityImpl IRole;
	
	@RequestMapping(value="/ajouterRole", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public RoleEntity ajouterRole(@RequestBody RoleEntity re) {
		return IRole.createRole(re);
	}
	
	@RequestMapping(value="/listeRole", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public List<RoleEntity> listeRole() {
		return IRole.readRole();
	}
	
	@PutMapping(value="/updateRolelById/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@Transactional
	public RoleEntity updateRoleById(@RequestBody RoleEntity re, @PathVariable Long id) {
		
		re.setId(id);
		role.save(re);
		return re;
		//return "Record updated successfully using @Modifiying and @query Named Parameter";
	
	}
	
	@GetMapping(value="/afficherUnRole/{id}")
	public RoleEntity afficherUnRole(@PathVariable Long id){
		
		RoleEntity re = role.findById(id).orElse(null);
		
		return re;
		
	}
	
	@DeleteMapping(value="/deleteUneRoleById/{id}")
	public void deleteUneRoleById(@PathVariable Long id) {
		IRole.deleteRoleById(id);
	}
	
}
