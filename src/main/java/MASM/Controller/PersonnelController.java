package MASM.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import MASM.Entity.PersonnelEntity;
import MASM.Repository.PersonnelRepository;
import MASM.Service.PersonnelEntityImpl;

@RestController
@CrossOrigin(origins = "http://localhost:4200")

public class PersonnelController {
	
	@Autowired
	PersonnelRepository personnel;
	
	@Autowired
	PersonnelEntityImpl IPersonnel;
	
	@RequestMapping(value="/ajouterPersonnel", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public PersonnelEntity ajouterPersonnel(@RequestBody PersonnelEntity pe) {
		return IPersonnel.createPersonnel(pe);
	}
	
	@RequestMapping(value="/listePersonnel", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public List<PersonnelEntity> listePersonnel() {
		return IPersonnel.readPersonnel();
	}
	
	@PutMapping(value="/updatePersonnelById/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@Transactional
	public PersonnelEntity updatePersonneltById(@RequestBody PersonnelEntity pe, @PathVariable Long id) {
		
		pe.setId(id);
		personnel.save(pe);
		return pe;
		//return "Record updated successfully using @Modifiying and @query Named Parameter";
	
	}
	
	@GetMapping(value="/afficherUnePersonne/{id}")
	public PersonnelEntity afficherUnePersonne(@PathVariable Long id){
		
		PersonnelEntity pe = personnel.findById(id).orElse(null);
		
		return pe;
		
	}
	
	@DeleteMapping(value="/deleteUnePersonne/{id}")
	public void deleteUnePersonneById(@PathVariable Long id) {
		IPersonnel.deletePersonnelById(id);
	}

}
