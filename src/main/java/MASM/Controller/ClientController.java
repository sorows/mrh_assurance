package MASM.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import MASM.Entity.ClientEntity;
import MASM.Repository.ClientRepository;
import MASM.Service.ClientEntityImpl;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ClientController {
	
	@Autowired
	ClientRepository client;
	
	@Autowired
	ClientEntityImpl IclientEntity;

	@RequestMapping(value="/ajouterClient", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ClientEntity ajouterClient(@RequestBody ClientEntity cl) {
		return IclientEntity.createClient(cl);
	}
	
	@RequestMapping(value="/listeClient", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public List<ClientEntity> listeAgence() {
		return IclientEntity.readClient();
	}
	
	@PutMapping(value="/updateClient/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@Transactional
	public ClientEntity updateClientById(@RequestBody ClientEntity cl, @PathVariable Long id) {
		
		cl.setId(id);
		client.save(cl);
		return cl;
		//return "Record updated successfully using @Modifiying and @query Named Parameter";
	
	}
	
	@GetMapping(value="/afficherUnClient/{id}")
	public ClientEntity afficherUnClient(@PathVariable Long id){
		
		ClientEntity cl = client.findById(id).orElse(null);
		
		return cl;
		
	}
	
	@DeleteMapping(value="/deleteUnClient/{id}")
	public void deleteUnClient(@PathVariable Long id) {
		IclientEntity.deleteClientById(id);
	}

}
