package MASM.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import MASM.Entity.ContactezNousEntity;
import MASM.Repository.ContactezNousRepository;
import MASM.Service.ContactezNousEntityImpl;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ContactezNousController {

	@Autowired
	ContactezNousRepository contact;
	
	@Autowired
	ContactezNousEntityImpl IContact;
	
	@RequestMapping(value="/ajouterContacts", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ContactezNousEntity ajouterContacts(@RequestBody ContactezNousEntity cne) {
		return IContact.createContactez(cne);
	}
	
	@RequestMapping(value="/listeContacts", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public List<ContactezNousEntity> listeContacts() {
		return IContact.readContactez();
	}
	
}
