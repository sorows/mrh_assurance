package MASM.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import MASM.Entity.ServiceEntity;
import MASM.Repository.ServiceRepository;
import MASM.Service.ServiceEntityImpl;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ServiceController {
	
	@Autowired
	ServiceRepository service;
	
	@Autowired
	ServiceEntityImpl IService;
	
	@RequestMapping(value="/ajouterService", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ServiceEntity ajouterService(@RequestBody ServiceEntity se) {
		return IService.createService(se);
	}
	
	@RequestMapping(value="/listeService", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public List<ServiceEntity> listeContrat() {
		return IService.readService();
	}
	
	@PutMapping(value="/updateServiceById/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@Transactional
	public ServiceEntity updateContratById(@RequestBody ServiceEntity se, @PathVariable Long id) {
		
		se.setId(id);
		service.save(se);
		return se;
		//return "Record updated successfully using @Modifiying and @query Named Parameter";
	
	}
	
	@GetMapping(value="/afficherUnService/{id}")
	public ServiceEntity afficherUnContrat(@PathVariable Long id){
		
		ServiceEntity se = service.findById(id).orElse(null);
		
		return se;
		
	}
	
	@DeleteMapping(value="/deleteUnService/{id}")
	public void deleteUnContrat(@PathVariable Long id) {
		IService.deleteServiceById(id);
	}

}
