package MASM.Controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import MASM.Entity.NewsLettersEntity;
import MASM.Repository.NewsLetttersRepository;
import MASM.Service.NewsLettersEntityImpl;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class NewsLetterController {
	
	@Autowired
	NewsLetttersRepository news;
	
	@Autowired
	NewsLettersEntityImpl INews;
	
	@RequestMapping(value="/ajouterNewsLetter", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public NewsLettersEntity ajouterNewsLetter(@RequestBody NewsLettersEntity ne) {
		return INews.createNewsLetter(ne);
	}
	
	@RequestMapping(value="/listeNewsLetter", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public List<NewsLettersEntity> listeNewsLetter() {
		return INews.readNewsLetter();
	}

}
