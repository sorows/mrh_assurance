package MASM.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import MASM.Entity.AgenceEntity;
import MASM.Repository.AgenceEntityRepository;
import MASM.Service.AgenceEntityImpl;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class AgenceController {
	
	@Autowired
	AgenceEntityRepository agence;
	
	@Autowired
	AgenceEntityImpl IagenceEntity;

	@RequestMapping(value="/ajouterAgence", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public AgenceEntity ajouterAgence(@RequestBody AgenceEntity ag) {
		return IagenceEntity.createAgence(ag);
	}
	
	@RequestMapping(value="/listeAgence", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public List<AgenceEntity> listeAgence() {
		return IagenceEntity.readAgence();
	}
	
	/*
	@PutMapping(value="/updateAgence", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public AgenceEntity updateAgence(@RequestParam String libelle, @RequestParam Long id) {
		return IagenceEntity.updateAgenceById(, id);
	}
	*/
	
	 /*
	//localhost:8083/updateAgence?libelle=MASM BONOUA&id=4
	@PutMapping(value="/updateAgence", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@Transactional
	public void updateAgenceById(@RequestParam("libelle") String lib,@RequestParam("id") Long id, Model model) {
		AgenceEntity ag = new AgenceEntity();
		ag.setLibelle(lib);
		ag.setId(id);
		agence.updateAgenceById(ag.getLibelle(), ag.getId());
		
		model.addAttribute("libelle", ag.getLibelle());
		model.addAttribute("id", ag.getId());
		
		//return "Record updated successfully using @Modifiying and @query Named Parameter";
	
	}
	*/
	
	@PutMapping(value="/updateAgence/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@Transactional
	public AgenceEntity updateAgenceById(@RequestBody AgenceEntity ag, @PathVariable Long id) {
		
		ag.setId(id);
		agence.save(ag);
		return ag;
		//return "Record updated successfully using @Modifiying and @query Named Parameter";
	
	}
	
	@GetMapping(value="/afficherUneAgence/{id}")
	public AgenceEntity afficherUneAgence(@PathVariable Long id){
		
		AgenceEntity ag = agence.findById(id).orElse(null);
		
		return ag;
		
	}
	
	@DeleteMapping(value="/deleteUneAgence/{id}")
	public void deleteUneAgence(@PathVariable Long id) {
		IagenceEntity.deleteAgenceById(id);
	}
	
}
